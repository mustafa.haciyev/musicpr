package org.example;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DatabaseConnector {
    private static final String DB_URL = "jdbc:mysql://localhost:3306/databaseconnector";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "12345";

    private Connection connection;

    public DatabaseConnector() {
        initializeDatabase();

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);
            System.out.println("Database connection established!");
        } catch (ClassNotFoundException e) {
            System.err.println("MySQL JDBC driver not found!");
        } catch (SQLException e) {
            System.err.println("Database connection error: " + e.getMessage());
        }
    }

    private void initializeDatabase() {
        try {
            connection = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

            String createTableQuery = "CREATE TABLE IF NOT EXISTS songs (" +
                    "id INT AUTO_INCREMENT PRIMARY KEY," +
                    "title VARCHAR(100) NOT NULL," +
                    "artist VARCHAR(100) NOT NULL," +
                    "duration INT NOT NULL" +
                    ")";

            Statement statement = connection.createStatement();
            statement.execute(createTableQuery);

            System.out.println("Database initialized.");

        } catch (SQLException e) {
            System.err.println("Error while initializing database: " + e.getMessage());
        }
    }


    public void closeConnection() {
        try {
            if (connection != null) {
                connection.close();
                System.out.println("Database connection closed.");
            }
        } catch (SQLException e) {
            System.err.println("Error while closing the database connection: " + e.getMessage());
        }
    }

    public void addSong(Song song) {
        try {
            String query = "INSERT INTO songs (title, artist, duration) VALUES (?, ?, ?)";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, song.getTitle());
            statement.setString(2, song.getArtist());
            statement.setInt(3, song.getDuration());
            statement.executeUpdate();
            System.out.println("Song added successfully.");
        } catch (SQLException e) {
            System.err.println("Error while adding song: " + e.getMessage());
        }
    }

    public List<Song> getAllSongs() {
        List<Song> songs = new ArrayList<>();
        try {
            String query = "SELECT * FROM songs";
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String title = resultSet.getString("title");
                String artist = resultSet.getString("artist");
                int duration = resultSet.getInt("duration");
                Song song = new Song(id, title, artist, duration);
                songs.add(song);
            }
        } catch (SQLException e) {
            System.err.println("Error while fetching songs: " + e.getMessage());
        }
        return songs;
    }

}
