package org.example;

import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        DatabaseConnector connector = new DatabaseConnector();

        Scanner scanner = new Scanner(System.in);
        System.out.print("Şarkı adı: ");
        String title = scanner.nextLine();
        System.out.print("Sanatçı adı: ");
        String artist = scanner.nextLine();
        System.out.print("Süre (saniye cinsinden): ");
        int duration = scanner.nextInt();

        Song song = new Song(0, title, artist, duration);
        connector.addSong(song);

        List<Song> songs = connector.getAllSongs();
        System.out.println("\nVeritabanındaki tüm şarkılar:");
        for (Song s : songs) {
            System.out.println(s.getTitle() + " - " + s.getArtist() + " (" + s.getDuration() + " saniye)");
        }

        connector.closeConnection();

        System.out.print("\nAranacak şarkı adı: ");
        String searchTitle = scanner.nextLine();

        boolean songExists = false;
        for (Song s : songs) {
            if (s.getTitle().equalsIgnoreCase(searchTitle)) {
                songExists = true;
                break;
            }
        }

        if (songExists) {
            System.out.println("Aradığınız şarkı veritabanında mevcut.");
        } else {
            System.out.println("Aradığınız şarkı veritabanında bulunamadı.");
        }

    }

}